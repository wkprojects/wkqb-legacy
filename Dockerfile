FROM perl:5

WORKDIR /usr/src/app
ENTRYPOINT [ "carton", "exec", "perl" ]

RUN mkdir -p /etc/ssl/custom
RUN cpanm Carton \
    && mkdir -p /usr/src/app
RUN apt-get update && apt-get -y install libsodium23 libsodium-dev && rm -rf /var/lib/apt/lists/*

COPY cpanfile* ./
COPY ca-cert.pem /etc/ssl/custom

RUN carton install

COPY bot.pl ./

